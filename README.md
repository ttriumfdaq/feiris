
# Iris frontend code

This repository contains the frontend code for the Iris experiment. In the src-
folder you find the code running on the two VME controllers reading out the ADC
modules and TDC modules. Building the project produces two executables,
feairis.exe and fetiris.exe.

## Compiling

Go to the src-folder and type 'make' in the terminal. This should compile the
new (2020) asynchronous frontend code, which is found in feiris.cxx. The older
frontend code, used up till 2020, still exists in the file feiris_ebye.cxx. You
can compile the older code by typing 'make ebye'.


