/***************************************************************************/
/*                                                                         */
/*  Filename: OdbV1190.h                                                   */
/*                                                                         */
/*  Function: headerfile for the CAEN V1190                                */
/*                                                                         */
/* ----------------------------------------------------------------------- */
/*                                                                         */
/***************************************************************************/

#ifndef  ODBV1190_INCLUDE_H
#define  ODBV1190_INCLUDE_H

typedef struct {
  INT       setup;
  INT       leresolution;
  INT       windowoffset;
  INT       windowwidth;
} V1190_CONFIG_SETTINGS;

#define V1190_CONFIG_SETTINGS_STR(_name) const char *_name[] = {\
"Setup = INT : 0", \
"LeResolution = INT : 0", \
"WindowOffset = INT : 0", \
"WindowWidth = INT : 0", \
NULL }
#endif  //  ODBV1190_INCLUDE_H
