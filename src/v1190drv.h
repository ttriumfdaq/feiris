/*********************************************************************

  Name:         v1190drv.h
  Created by:   Pierre-Andre Amaudruz

  Contents:     V1190B 64ch. or 128ch TDC

  $Id: v1190B.h 4613 2009-10-24 04:14:13Z olchanski $
*********************************************************************/

#include "mvmestd.h"

#include "v1190.h"

#ifndef V1190DRV_INCLUDE_H
#define V1190DRV_INCLUDE_H

#ifdef __cplusplus
extern "C" {
#endif

int  udelay(int usec);
WORD v1190_Read16(MVME_INTERFACE *mvme, DWORD base, int offset);
DWORD v1190_Read32(MVME_INTERFACE *mvme, DWORD base, int offset);
void v1190_Write16(MVME_INTERFACE *mvme, DWORD base, int offset, WORD value);
int  v1190_EventRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int *nentry);
int  v1190_DataRead(MVME_INTERFACE *mvme, DWORD base, DWORD *pdest, int nentry);
void v1190_SoftReset(MVME_INTERFACE *mvme, DWORD base);
void v1190_SoftClear(MVME_INTERFACE *mvme, DWORD base);
void v1190_SoftTrigger(MVME_INTERFACE *mvme, DWORD base);
void v1190_DataReset(MVME_INTERFACE *mvme, DWORD base);
int  v1190_DataReady(MVME_INTERFACE *mvme, DWORD base);
int  v1190_EvtCounter(MVME_INTERFACE *mvme, DWORD base);
int  v1190_EvtStored(MVME_INTERFACE *mvme, DWORD base);
int  v1190_MicroCheck(MVME_INTERFACE *mvme, const DWORD base, int what);
int  v1190_MicroWrite(MVME_INTERFACE *mvme, DWORD base, WORD data);
int  v1190_MicroRead(MVME_INTERFACE *mvme, const DWORD base);
int  v1190_MicroFlush(MVME_INTERFACE *mvme, const DWORD base);
void v1190_TdcIdList(MVME_INTERFACE *mvme, DWORD base);
int  v1190_ResolutionRead(MVME_INTERFACE *mvme, DWORD base);
void v1190_LEResolutionSet(MVME_INTERFACE *mvme, DWORD base, WORD le);
void v1190_LEWResolutionSet(MVME_INTERFACE *mvme, DWORD base, WORD le, WORD width);
void v1190_TriggerMatchingSet(MVME_INTERFACE *mvme, DWORD base);
void v1190_AcqModeRead(MVME_INTERFACE *mvme, DWORD base);
void v1190_ContinuousSet(MVME_INTERFACE *mvme, DWORD base);
void v1190_WidthSet(MVME_INTERFACE *mvme, DWORD base, WORD width);
void v1190_OffsetSet(MVME_INTERFACE *mvme, DWORD base, WORD offset);
int  v1190_GeoWrite(MVME_INTERFACE *mvme, DWORD base, int geo);
int  v1190_Setup(MVME_INTERFACE *mvme, DWORD base, int mode);
int  v1190_Status(MVME_INTERFACE *mvme, DWORD base);
void v1190_SetEdgeDetection(MVME_INTERFACE *mvme, DWORD base, int eLeading, int eTrailing);

#ifdef __cplusplus
}
#endif
#endif // V1190DRV_INCLUDE_H
