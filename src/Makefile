#####################################################################
#
#  Name:         Makefile
#  Created by:   Stefan Ritt
#
#  Contents:     UNIX Makefile for MIDAS slow control frontend
#
#  $Id: Makefile 2779 2005-10-19 13:14:36Z ritt $
#
#####################################################################

#--------------------------------------------------------------------
# The following lines contain specific switches for different UNIX
# systems. Find the one which matches your OS and outcomment the 
# lines below.

# This is for Linux ----------------
LIBS = -lbsd -lm -lutil -lpthread -lrt
OSFLAGS = -DOS_LINUX

# This is for OSF1 -----------------
#LIBS = -lbsd
#OSFLAGS = -DOS_OSF1

# This is for Ultrix ---------------
#LIBS =
#OSFLAGS = -DOS_ULTRIX -Dextname

# This is for FreeBSD --------------
#LIBS = -lbsd -lcompat
#OSFLAGS = -DOS_FREEBSD

#-------------------------------------------------------------------
# The following lines define direcories. Adjust if necessary
#                 
INC_DIR 	= $(MIDASSYS)/include
LIB_DIR 	= $(MIDASSYS)/linux/lib
DRV_DIR		= $(MIDASSYS)/drivers
KO_DIR          = $(MIDASSYS)/../vme
IRIS_DIR  = $(HOME)/packages/iris-daqtools
# VME_DIR         = /home/olchansk/daq/vmisft-7433-3.6-KO3/vme_universe
# VMEDRV = vmicvme.o
# VMELIB = -lvme
VMEDRV = gefvme.o
#-------------------------------------------------------------------
# Drivers needed by the frontend program
#                 
DRIVERS         = mesadc32.o v1190B.o VMENIMIO32.o

####################################################################
# Lines below here should not be edited
####################################################################

LIB = $(LIB_DIR)/libmidas.a $(VMELIB) $(IRIS_DIR)/build/libdaqtools_static.a

# compiler
CC = gcc
CXX = g++
CFLAGS = -std=c++11 -g -Wall -DUNIX -I. -I$(INC_DIR) -I$(KO_DIR) -I$(DRV_DIR)/vme -I$(DRV_DIR)/vme/vmic -I$(IRIS_DIR)/include
LDFLAGS =

FECODE = feiris.cxx

all:: feairis.exe
all:: fetiris.exe

feairis.exe: $(LIB) $(LIB_DIR)/mfe.o feiris-gef-adc.o $(VMEDRV) $(DRIVERS)
	$(CXX) -o $@ feiris-gef-adc.o $(LIB_DIR)/mfe.o $(VMEDRV) $(DRIVERS) $(LIB) $(LDFLAGS) $(LIBS)

fetiris.exe: $(LIB) $(LIB_DIR)/mfe.o feiris-gef-tdc.o $(VMEDRV) $(DRIVERS)
	$(CXX) -o $@ feiris-gef-tdc.o $(LIB_DIR)/mfe.o $(VMEDRV) $(DRIVERS) $(LIB) $(LDFLAGS) $(LIBS)

feiris.exe: $(LIB) $(LIB_DIR)/mfe.o feiris.o mesadc32.o $(VMEDRV) $(DRIVERS)
	$(CXX) -o $@ feiris.o mesadc32.o $(LIB_DIR)/mfe.o $(VMEDRV) $(DRIVERS) $(LIB) $(LDFLAGS) $(LIBS)

mesadc32.exe:  mesadc32main.o $(VMEDRV)
	$(CC) $(CFLAGS)  -o mesadc32.exe mesadc32main.o $(VMEDRV) $(LDFLAGS) $(VMELIB) $(LIB) $(LIBS)

#feiris.o: feiris.cxx $(IRIS_DIR)/include/*.h
feiris.o: $$(FECODE) $(IRIS_DIR)/include/*.h
	@echo "feiris.o"
	@echo $(FECODE)
	$(CXX) $(CFLAGS) $(OSFLAGS) -c $(FECODE) -o $@

#feiris-gef-adc.o: feiris.cxx $(IRIS_DIR)/include/*.h
.SECONDEXPANSION:
feiris-gef-adc.o: $$(FECODE) $(IRIS_DIR)/include/*.h
	@echo "feiris-gef-adc.o"
	@echo $(FECODE)
	$(CXX) -DMESADC32_CODE -DHAVE_IO32_ADC $(CFLAGS) $(OSFLAGS) -c $(FECODE) -o $@

#feiris-gef-tdc.o: feiris.cxx $(IRIS_DIR)/include/*.h
feiris-gef-tdc.o: $$(FECODE) $(IRIS_DIR)/include/*.h
	@echo "feiris-gef-tdc.o"
	@echo $(FECODE)
	$(CXX) -DV1190_CODE -DHAVE_IO32_TDC $(CFLAGS) $(OSFLAGS) -c $(FECODE) -o $@

#mesadc32main.o: $(DRV_DIR)/vme/mesadc32.c
mesadc32main.o: mesadc32.c
	$(CC) -DMAIN_ENABLE $(CFLAGS) $(OSFLAGS) -c $< -o $@

#mesadc32.o: $(DRV_DIR)/vme/mesadc32.c
mesadc32.o: mesadc32.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@

v1190B.o: v1190B.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@

VMENIMIO32.o: $(KO_DIR)/VMENIMIO32.cxx
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@

vmicvme.o : $(DRV_DIR)/vme/vmic/vmicvme.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $< -o $@

#Private version of gefvme.c
gefvme.o: gefvme.c
	$(CC) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

#gefvme.o: $(DRV_DIR)/vme/vmic/gefvme.c
#	$(CC) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

#This target is for compiling the event-by-event daq code (pre-2020).
ebye: FECODE = feiris_ebye.cxx
ebye: all

%.o: %.cxx experim.h
	$(CXX) $(USERFLAGS) $(ROOTCFLAGS) $(CFLAGS) $(OSFLAGS) -o $@ -c $<

.c.o:
	$(CXX) $(CFLAGS)  $(OSFLAGS) -c $<

clean::
	rm -f *.o *.exe

#end
